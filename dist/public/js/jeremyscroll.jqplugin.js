//jeremy-scroll.jqplugin.js

(function($){
    var containerHeight = 0;
    var scrollHeight = 0;
    var $window = $(window);
    var targetHeight = 0
    var $elem = null;
    var $scrollBar = null;
    var $shuttle = null;
    var shuttleDistance = 0;
    var origImgWidth = 0;
    var origImgHeight = 0;
    var shuttleWidth = 0;
    var shuttleHeight = 0;
    var shuttleY = 0;
    var mouseY = 0;
    var isDragging = false;
    $.fn.jeremyScroll = function(elem, target, side, shuttleImage, shuttleImgWidth, shuttleImgHeight) {
        var _this = this;
        var img = new Image();
        img.onload = function() {
            origImgWidth = this.width;
            origImgHeight = this.height;
            targetHeight = target;
            shuttleWidth = shuttleImgWidth ? shuttleImgWidth : origImgWidth;
            shuttleHeight = shuttleImgHeight ? shuttleImgHeight : origImgHeight;
            containerHeight = parseInt($(_this).height());
            scrollHeight = containerHeight - targetHeight;
            $elem = $(elem);
            shuttleDistance = containerHeight - shuttleHeight;
            configureContainer(_this, targetHeight);
            configureElement(_this, elem);
            var $jeremyBar = getScrollBar(side, shuttleImage, shuttleWidth, shuttleHeight);
            $(_this).prepend($jeremyBar);

            $shuttle.mousedown(function(event){
                onMouseDown(event);
            });

            $scrollBar.click(function(event){
                onScrollBarClick(event);
            })
            return _this;
        }
        img.src = shuttleImage;
    }

    function onScrollBarClick(event) {
        if($(event.srcElement).attr('id') == 'jeremy-scrollbar') {
            var clickY = event.offsetY;
            var shuttleTop = parseInt($shuttle.css('top'));
            var shuttleBottom = shuttleTop + shuttleHeight;

            if (clickY < shuttleTop) {
                var newTop = shuttleTop - shuttleHeight < 0 ? 0 : shuttleTop - shuttleHeight;
            }
            else if (clickY > shuttleBottom) {
                var newTop = shuttleBottom > (targetHeight - shuttleHeight) ? (targetHeight - shuttleHeight) : shuttleBottom;
            }
            $shuttle.css('top', newTop);
            scrollElement(newTop, (targetHeight - shuttleHeight));
        }
    }

    function onMouseDown(downEvent) {
        $("body").disableSelection();
        isDragging = true;
        var startY = parseInt($shuttle.css("top"));
        if(isNaN(startY)){
            startY = 0;
        }

        var onDrag = function(moveEvent){
            onMouseMove(moveEvent, downEvent, startY)
        };
        var onDragStop = function(upEvent){
           onMouseUp(upEvent, downEvent);
        };

        $shuttle
            .mousemove(onDrag)
            .mouseup(onDragStop)

        $(window)
            .mouseup(onDragStop)
            .mouseleave(onDragStop);
    }

    function onMouseMove(moveEvent, downEvent, startY) {
        if(isDragging) {
            var dy = moveEvent.screenY - downEvent.screenY;
            var maxY =  (targetHeight - shuttleHeight);
            var newY = startY + dy;

            if(newY >= 0 && newY <= maxY) {
                console.log('new ' + newY + ' shuttle ' + shuttleHeight + ' max ' + maxY)
                $shuttle.css({
                    top: newY
                });
                scrollElement(newY, maxY);
            }
        }
    }

    function onMouseUp(upEvent, downEvent) {
        this.dragging = false;
        $shuttle
            .unbind("mousemove")
            .unbind("mouseup")
            .unbind("mouseleave");
        $(window)
            .unbind("mouseup")
            .unbind("mouseleave");
    }

    function scrollElement(shuttleY, maxY) {
        var percentage = (shuttleY / maxY);
        var top = -1 * percentage * scrollHeight;
        $elem.css('top', top);
    }

    function configureContainer(elem, targetHeight) {
        $(elem).css({
            'height': targetHeight + 'px',
            'overflowY': 'hidden'
        })
    }

    function configureElement(container, elem) {
        $(container).find(elem).css('position', 'absolute');
    }

    function getScrollBar(side, shuttleImage, shuttleWidth, shuttleHeight) {
    	$scrollBar = $('<div>')
			.attr('id', 'jeremy-scrollbar')
			.css({
	    		'position': 'relative',
	    		'display': 'block',
	    		'float': side,
	    		'height': '100%',
                'width': shuttleWidth,
                'cursor': 'pointer'
	    	});
    	$shuttle = $('<div>')
    		.attr('id', 'jeremy-shuttle')
    		.css({
    			'position': 'absolute',
    			'display': 'block',
    			'top': 0 + 'px',
    			'width': shuttleWidth + 'px', 
    			'height': shuttleHeight + 'px',
    			'backgroundImage': 'url(' + shuttleImage + ')',
    			'backgroundSize': '100% 100%'
    		})
    		// calling css once more so that we can set the left/right dynamically.
    		.css(side, 0 + 'px');

    	$scrollBar.append($shuttle);
        return $scrollBar[0];
    }






})(jQuery);
