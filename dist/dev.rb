require 'sinatra'

class Web < Sinatra::Base
	use Rack::Deflater

  # use Rack::Auth::Basic, "Restricted Area" do |username, password|
  # [username, password] == ['dc', 'brau']
  # end

  # configure :production do
  # 	require 'newrelic_rpm'
  # end

  # before do
  #   headers["Access-Control-Allow-Origin"] = "*"
  #   headers["Access-Control-Allow-Methods"] = "GET"
  #   cache_control :public, :max_age => 60 * 60 * 24 * 365
  # end

  get '/' do
  	File.read(File.join('public', 'index.html'))
  end

end
Web.run!