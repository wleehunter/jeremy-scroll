require 'sinatra'

use Rack::Auth::Basic, "Restricted Area" do |username, password|
  [username, password] == ['dc', 'brau']
end

get '/' do
  File.read(File.join('public', 'index.html'))
end
