# README #

Jeremy Scroll -

This dude that I work with, Jeremy, is a UX designer. He really hates default scrollbars and generally prefers a custom solution. So I made this jquery plugin, $.jeremyScroll(), for him so that we can easily attach custom scrollbars. You can specify a bunch of characteristics (like the image for the shuttle). But obviously you should just stick with Jeremy's image.

          /**
           *  Arguments:
           *  @param {string} elem The element to scroll
           *  @param {number} targetHeight the height for scrollable content
           *  @param {string} side that you want the scrollbar to be on
           *  @param {string} shuttleImage to be used for the shuttle. Path or url
           *  @param {number} shuttleImgWidth Optional. It'll default to the image's natural size.
           *  @param {number} shuttleImgHeight Optional. It'll default to the image's natural size.
           *
          **/

          $('#content').jeremyScroll('ul', 200, 'right', '/images/jeremy.png', 50, 65);